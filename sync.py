# -*- coding: utf-8 -*-
import fLog
fLog.rotated_log_object = fLog.Basics()
cfg = {
      "log_filename": "C:\\Users\\admin\\Desktop\\si_city_sync\\city_sync.txt",
      "log_level": 10,
      "log_show_in_console": False,
      "log_maxMb" : 10 ,
      "log_rotate" : 10
    }
fLog.rotated_log_object.init(cfg) 

print("ok")
import pyodbc 
import json
import urllib3
import certifi
import time
import lkcore

region_id = 78
#url = 'http://rcv.plfn.org/crm'
#url = 'http://81.23.102.26:11110/crm'
url = 'http://192.168.29.144:10010/crm'

#city KB
#ConnectionString_Objects=Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=Data_Objects_Device_2;Data Source=127.0.0.1,1433

#LO KB
#ConnectionString_Tech=Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=Data_Tech_Device_2;Data Source=LENOBL,1433

#CONN_STR = 'Driver={SQL Server};Server=LENOBL;Database=Data_Objects_Device_2;Trusted_Connection=yes;'
CONN_STR = 'Driver={SQL Server};Server=127.0.0.1;Database=Data_Objects_Device_2;Trusted_Connection=yes;'

def send_json(js):
    global url
    http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())

    encoded_data = json.dumps(js).encode('utf-8')
    

    try:
        r = http.request('GET', url,body=encoded_data, headers={'Content-Type': 'application/json', 'Connection': 'Keep-Alive'}, timeout = 3)
        print("Answered from {}".format(url))
        if r.status == 200:
            js_result = json.loads(r.data.decode('utf-8'))

            return 1             
    except Exception as e:
        print("CORE:send_json() Exception {}".format(e))

    return 0
    
def send_object_crm(row, columns):
    global region_id
    res = dict(dict( zip(columns, row) ))
    print("Sending...")
    print(res)    
    crm_test(res)
    print("DONE")

def get_rows():
    conn = pyodbc.connect(CONN_STR)
    cursor = conn.cursor()      
    cursor.execute('''SELECT 
      DEMANDS.[ID_OBJECT] as ID_DEMAND
      ,ID_DEVICE
      ,DEMANDS.[TITLE]
      ,DEMANDS.[TITLE_OBJECT] as ABBREVIATION
      ,DEMANDS.[ID_DISTRICT] as ID_DISTRICT_OBJECT
      ,DEMANDS.[SIM_MEGAFON] as SIM1
      ,DEMANDS.[SIM_MTS] as SIM2
      ,OBJECTS.[STATUS_OBJ]
      ,OBJECTS.CONTACT_PHONE_1 as PHONE, 
      OBJECTS.CONTACT_PHONE_2 as STANDBY_PHONE, 
      OBJECTS.NAME_1 as DIRECTOR, 
      OBJECTS.ROUTING, OBJECTS.BLOCKING
      
      FROM [Data_Objects_Device_2].[dbo].[DEMANDS] LEFT JOIN OBJECTS ON OBJECTS.ID_OBJECT = DEMANDS.ID_OBJECT WHERE OBJECTS.STATUS_OBJ > 0''')      

    i = 0
    columns = [column[0] for column in cursor.description]
    #print(columns)

    print("\n\nStart Querry objects...")
    for row in cursor:
        i += 1
        #print("row :{}".format(i))
        #print(row)
        #if i == 200:
        #     return
        #if( int(row[0]) == 1127 ): 
        #    print( row ) 
        send_object_crm(row, columns)
        #time.sleep(0.3)
 
        

def crm_test(obj_info):
    import urllib.request
    import json

    
    data = {"Table": "DEMANDS",
        "Data": obj_info,
        "Region": 78
    }  
    
    url = "https://devlk.plfn.org:3000/api/sync_city"
    data = json.dumps(data).encode("utf-8") # convert data to bytes
    headers = {"Content-Type": "application/json"}
    request = urllib.request.Request(url, data=data, headers=headers)
    response = urllib.request.urlopen(request)
    print(response.read().decode("utf-8")) # print the response from the server
    

# obj_info = {"ID_DEMAND"    :"...", "TITLE"    :"...", "ABBREVIATION"    :"...", "ID_DISTRICT_OBJECT"    :"...", "LOCALITY_OBJECT"    :"...", "Type_Street"    :"...","STREET_TITLE_OBJECT"    :"...", "HOME_NUMBER_OBJECT"    :"...", "BUILDING_NUMBER_OBJECT"    :"...", "ROOM_NUMBER_OBJECT"    :"...", "MAN_FIRE_SAFETY"    :"...", "DIRECTOR"    :"...", "MOBILE_MAN_FIRE_SAFETY"    :"...", "PHONE"    :"...", "PHONE_MAN_FIRE_SAFETY"    :"...", "STANDBY_PHONE"    :"...", "ID_EQUIPMENT"    :"...", "ID_EQUIPMENT2"    :"...", "ID_EQUIPMENT3"    :"...", "ID_EQUIPMENT4"    :"...", "STATUS_OBJ"    :"...", "ID_OBJECT"    :"...", "OBJECT_NAME"    :"...", "Block_history"    :"..."}
    
# data = {"Table": "object",
    # "Data": obj_info,
    # "Region": 78
# }    
# crm_test(data)

get_rows()

