import urllib3
import json
import certifi
from fLog import log, ex
import urllib.request

rest = 'http://devlk.plfn.org:3000'  

def get_param( js, param, default ):
    if param in js:
        return js[param]
    return default
    
def get_server_link(sub_link):
    global rest
    return '{}/api/{}'.format(rest, sub_link)

def get_http():    
    #http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())
    http = urllib3.PoolManager()
    return http


  #Get token from server 
def auth():
	#print("Requesting REST server...")
	js = {"email":"ppo-pp@plfn.org", "password":"opfVOVCwQUpS76YD"}
	
	http = get_http()
	encoded_data = json.dumps(js).encode('utf-8')
	try:
		r = http.request('POST', get_server_link('auth/signin'), body=encoded_data, headers={'Content-Type': 'application/json', 'Connection': 'Keep-Alive'}, timeout = 3, retries=urllib3.Retry(3, redirect=2))
	except Exception as e:
		log("Fail to uath in LK via {}.\n Reason: {}".format(get_server_link('auth/signin'), e), 'REST', True)
		return None
	
	if( r.status != 200 ):
		print(" NOT ALLOWED ")
		return None
	   
	js_result = json.loads(r.data.decode('utf-8'))
	   
	token = get_param( js_result, 'token', None )
	uname = get_param( js_result, 'name', 'UNKNOWN USER' )
	
	print(f"Hello {uname}")
	return token
    
def send_lk_request_raw(token, params, lk_addr, method = 'GET'):
    if token is None:
        return (0, 0, '')
        # if not self.auth():
            # log('Error auth in REST', 'REST')
            # return (0, 0, '')
    
    
    #http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())
    http = get_http()
    
    try:
        link = get_server_link( lk_addr )
        log('requesting {} : [{}]'.format(link, params), 'LK', True)
        r = http.request(method, link, fields = params, headers={'Content-Type': 'application/json', 'Connection': 'Keep-Alive', 'x-access-token' : token}, timeout = 30, retries=urllib3.Retry(3, redirect=2))
        js_result = ''
        if r.status == 403:
            log('AUTH FAIL', 'LK', False)
            return (0, r.status, '')
            #self.auth()
            #r = http.request('GET', link, fields = params, headers={'Content-Type': 'application/json', 'Connection': 'Keep-Alive', 'x-access-token' : self.token}, timeout = 30, retries=urllib3.Retry(3, redirect=2))
        if r.status == 200:
            if( len(r.data) > 4096 ):
                log('OK, size=<{}>'.format(len(r.data)), 'LK', False)
            else:
                log('OK, size=<{}>, data = <{}>'.format(len(r.data), r.data), 'LK', False)
            return (1, r.status, r.data )
        else:
            log("Unknown error:{}\n{}".format(r.status, r.data), 'LK')

        return (0, r.status, '' )                
    
    except Exception as e:
        ex("Fail to request  via {}".format(link), e)
        
    return (0, 0, '')

    
def send_lk_request(token, params, lk_addr, method = 'GET'):
    res = send_lk_request_raw(token, params, lk_addr, method)
    if res[0] == 1:
        try:
            return (res[0], res[1], json.loads(res[2].decode('utf-8')))
        except Exception as e:
            ex('send_lk_request', e)
    else:
        return (res[0], res[1], None)   

def push_crm(token, data):
    headers = {"Content-Type": "application/json"}
    request = urllib.request.Request(rest, data=data, headers=headers)
    response = urllib.request.urlopen(request)
    print(response.read().decode("utf-8")) # print the response from the server
    
   
def crm_test():
    import urllib.request
    import json

    url = "http://devlk.plfn.org:3000/api/sync_city"
    data = {"Table": "object",
        "Data": {"ID_DEMAND"    :"...", "TITLE"    :"...", "ABBREVIATION"    :"...", "ID_DISTRICT_OBJECT"    :"...", "LOCALITY_OBJECT"    :"...", "Type_Street"    :"...","STREET_TITLE_OBJECT"    :"...", "HOME_NUMBER_OBJECT"    :"...", "BUILDING_NUMBER_OBJECT"    :"...", "ROOM_NUMBER_OBJECT"    :"...", "MAN_FIRE_SAFETY"    :"...", "DIRECTOR"    :"...", "MOBILE_MAN_FIRE_SAFETY"    :"...", "PHONE"    :"...", "PHONE_MAN_FIRE_SAFETY"    :"...", "STANDBY_PHONE"    :"...", "ID_EQUIPMENT"    :"...", "ID_EQUIPMENT2"    :"...", "ID_EQUIPMENT3"    :"...", "ID_EQUIPMENT4"    :"...", "STATUS_OBJ"    :"...", "ID_OBJECT"    :"...", "OBJECT_NAME"    :"...", "Block_history"    :"..."},
        "Region": 47
    }
    data = json.dumps(data).encode("utf-8") # convert data to bytes
    headers = {"Content-Type": "application/json"}
    request = urllib.request.Request(url, data=data, headers=headers)
    response = urllib.request.urlopen(request)
    print(response.read().decode("utf-8")) # print the response from the server

   
   
def get_cam_list(token, mac):
    print('\n\n\n\nCAMS')
    return send_lk_request(token,  [], 'getWorkCamByMac/{}'.format(mac) )    
    
   