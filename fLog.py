import time
import os
import logging
import logging.handlers
import sys
import datetime

import linecache
import traceback

try:
    from termcolor import colored
except:
    pass

default_print = True
DEBUG_PRINT = True
DEBUG_PRINT = False

rotated_log_object = None
  
def pd( s, src = '' ):
    if( DEBUG_PRINT ):
        print('{}:{}:{}'.format( datetime.datetime.now().strftime("%H:%M:%S"), src, s)     )
  
def log(s, src = 'UNKN', is_print = default_print, color = 'gray'):
    try:
        global rotated_log_object
        if rotated_log_object is not None:
            rotated_log_object.log.error(' {}\t| {}'.format(src, s))
            if is_print:
                try:
                    if type(s) == 'str':
                        s = s.encode("utf8")
                except Exception as e:
                    print('FAIL s = s.encode("utf8") in log : {}'.format(e))
                    
                try:
                    print(colored('{}:{}:{}'.format( datetime.datetime.now().strftime("%H:%M:%S"), src, s), color)     )
                except:
                    print('{}:{}:{}'.format( datetime.datetime.now().strftime("%H:%M:%S"), src, s)     )
        else:
            rotated_log_object = Basics()
            cfg = {
                  "log_filename": "log_default.txt",
                  "log_level": 10,
                  "log_show_in_console": False,
                  "log_maxMb" : 10 ,
                  "log_rotate" : 10
                }
            rotated_log_object.init(cfg)  

            print('LOG INIT ERROR. Created default log_default.txt')
            rotated_log_object.log.error('| {}\t| {}'.format(src, s))
            print('{}:{}'.format(src, s))
    except Exception as e:
        ex('flog_ex', e)
        try:
            print("EXCEPRION[fLog.log] Something wrong in code: {}".format(e) )
        except:
            print("Exception, but I Can't print it")
   
# exc_type, exc_obj, tb = sys.exc_info()
# f = tb.tb_frame
# lineno = tb.tb_lineno
# filename = f.f_code.co_filename
# linecache.checkcache(filename)
# line = linecache.getline(filename, lineno, f.f_globals)
# out_text = u'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)
# out_text +=  u'TB:'+traceback.format_exc()
# print out_text
   
def ex(s, e, src = ''):
    try:
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        out_text = u'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)
        out_text +=  u'TB:'+traceback.format_exc()
        try:
            print(out_text)
        except:
            print("EX! SRC = {} Error in fLog.ex: {}".format(src, e) )

        log(out_text, 'EXCEPTION INFO', color = 'red' )
        log( "EX!:{}: {}|{}|{}".format( s, sys.exc_info()[-1].tb_lineno, type(e), e), src, True, color = 'red') 
    except Exception as e:
        print("EX! Error in fLog.ex: {}".format(e) )


class MyFormatter(logging.Formatter):
    converter=datetime.datetime.fromtimestamp
    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            s = ct.strftime(datefmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s.%02d" % (t, int(record.msecs/10))
        return s

class Basics:
    def init(self, config):
        self.config = config

        if not os.path.exists(os.path.dirname(self.config['log_filename'])):
            os.makedirs(os.path.dirname(self.config['log_filename']))

        FORMAT = "%(asctime)-15s %(message)s"
        logging.basicConfig(filename=self.config['log_filename'], format=FORMAT)
        
        
        #logging.basicConfig(format="%(asctime)s|%(levelname)s: %(message)s", level=self.config['log_level'], datefmt='%Y-%m-%d %H:%M:%S')
        logging.basicConfig(filename=self.config['log_filename'], format="%(asctime)s|NOT MY\t| %(message)s", level=self.config['log_level'], datefmt='%Y-%m-%d %H:%M:%S.xx')
        logging.getLogger('requests').setLevel(self.config['log_level'])
        self.log = logging.getLogger('status_pusher')
        self.log.propagate = self.config['log_show_in_console']
        #formatter = logging.Formatter("%(asctime)s|%(levelname)s: %(message)s", datefmt='%Y-%m-%d %H:%M:%S')
        formatter = MyFormatter("%(asctime)s|%(message)s")
        lh_max_bytes = (self.config['log_maxMb']*1000000)
        lh_b_count = self.config['log_rotate']
        log_handler = logging.handlers.RotatingFileHandler(self.config['log_filename'], maxBytes = lh_max_bytes, backupCount = lh_b_count)
        print("LOG {} Created. MB = {}, Count = {}".format(self.config['log_filename'], lh_max_bytes, lh_b_count))
        log_handler.setLevel(self.config['log_level'])
        log_handler.setFormatter(formatter)
        self.log.addHandler(log_handler)

        log('START', 'START')


        
        
